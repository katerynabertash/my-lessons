//Теоретический вопрос
//Document Object Model (DOM) - это дерево всех html элементов, где каждый html элемент представлен узлом

function viewPage(items, target = document.body) {
  target.innerHTML = items.map((item) => `<li>${item}</li>`).join("");
}

viewPage(
  ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"],
  document.getElementById("list")
);
