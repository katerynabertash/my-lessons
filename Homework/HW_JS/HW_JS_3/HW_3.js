//Теоретический вопрос
//1. Функции нужны в программировании для повторного использования кода.
//2. Функция должна что-то делать, а параметри или аргументи передають значения которие при изменениях не отразятся после вызова функции.
let firstNumber = +prompt("Введите первое число:");
let secondNumber = +prompt("Введите второе число:");
let operation = prompt("Введите операцию (например: +, -, *, /)");

function calcNambers(num1, num2, opp) {
  if (opp === "+") {
    return num1 + num2;
  } else if (opp === "-") {
    return num1 - num2;
  } else if (opp === "*") {
    return num1 * num2;
  } else if (opp === "/") {
    return num1 / num2;
  }
}

let res = calcNambers(firstNumber, secondNumber, operation);
console.log(`${firstNumber} ${operation} ${secondNumber} = ${res}`);
