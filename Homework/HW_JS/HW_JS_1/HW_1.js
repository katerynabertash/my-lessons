// Теоретический вопрос

// 1. Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
//var - считается устаревшим способом обьявления переменных, ее отличие в том что она не зависить от блоков. Переменные обьявление с  помощью let и const живут у блоках. let и var могут изменяться, const - нет.

// 2. Почему объявлять переменную через var считается плохим тоном?
// Потому что принято чтобы переменные жили в блоках, а var выходит за пределы блоков. Поэтому можно сказать, что var - это пережиток прошлого.

let firstName = prompt("What is your name?");
let age = prompt("How old are you?");

if (age < 18) {
  alert("You are not allowed to visit this website");
} else if (age <= 22) {
  confirm("Are you sure you want to continue?");
} else {
  alert(`Welcome ${firstName}`);
}
