//Теоретический вопрос
// Экранирование это использование управляющих символов как контента. Они нужны что бы вставить управляющий символ у контент.
function createNewUser() {
  let firstName = prompt("What is your first name?");
  let lastName = prompt("What is your last name?");
  let birthday = prompt("What is your birthday(dd.mm.yyyy)?");
  let yearBirthday = birthday.split(".")[2];
  let newUser = {
    firstName: firstName,
    lastName: lastName,
    getAge: function () {
      let currentYear = new Date().getFullYear();
      return currentYear - yearBirthday;
    },
    getPassword: function () {
      return (
        firstName.toUpperCase().charAt(0) +
        lastName.toLowerCase() +
        yearBirthday
      );
    },
    getLogin: function () {
      let mail = firstName.toLowerCase().charAt(0) + lastName.toLowerCase();
      return mail;
    },
  };
  return newUser;
}
const user = createNewUser();
console.log(user.getPassword());
console.log(user.getAge());
console.log(user.getLogin());
