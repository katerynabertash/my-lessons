//Теоретический вопрос
//Метод forEach это функция которая есть у массива.
// forEach принимает в параметер функцию которая проходиться по каждому елементу массива.

function filterBy(array, type) {
  let result = [];
  for (let item of array) {
    if (typeof item !== type) {
      result.push(item);
    }
  }
  return result;
}

const myArray = ["hello", "world", 23, "25", null];
const filteredItems = filterBy(myArray, "string");
console.log(filteredItems);
