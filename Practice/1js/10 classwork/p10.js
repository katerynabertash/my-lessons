function printRepeat(str, count) {
  if (typeof str !== "string" || !Number.isInteger()) {
    throw new Error();
  }
  let result = " ";
  for (let i = 0; i < count; i++) {
    result += str;
  }
  return result;
}
console.log(printRepeat("Kate", 6));
console.log(printRepeat(1, 2));
console.log(printRepeat("Andrej", 2));
