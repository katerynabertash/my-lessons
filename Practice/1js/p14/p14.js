// document.getElementById находим html обьект из страницы
// html обьект:
//      - textContent
//      - innerHTML
//      - addEventListener('ИМЯ_СОБЫТИЯ', функцияОбработчик) - функция для событий (клик)
//      - classList
// classList - обьект для работы с классам у HTML обьекта
//      - add(className)
//      - remove(className)

// contains - содержит ли в себе

const taskElem = document.getElementById("task1");
let bul = 0;
taskElem.addEventListener("click", () => {
  if (bul === 0) {
    bul = 1;
    taskElem.classList.add("completed");
  } else if (bul === 1) {
    bul = 0;
    taskElem.classList.remove("completed");
  }
  console.log(bul);
});
