let a = 1350;
let b;
if (a < 200) {
  b = a + 20;
} else if (a <= 1000) {
  b = a + a * 0.1;
} else {
  b = a + a * 0.07;
}

console.log(`Ваш чек ${a} грн, с чаем ${b.toFixed(1)}грн`);
