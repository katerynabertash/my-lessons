let myButton = document.querySelector(".counter");

myButton.addEventListener("mousedown", increment);
myButton.addEventListener("mouseup", increment);

let counter = 1;
function increment() {
  counter += 1;
  myButton.textContent = counter + "";
}
