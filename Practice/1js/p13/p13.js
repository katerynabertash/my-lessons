// document.getElementById находим html обьект из страницы
// html обьект:
//      - textContent
//      - innerHTML
//      - addEventListener('ИМЯ_СОБЫТИЯ', функцияОбработчик) - функция для событий (клик)
// classList - обьект для работы с классам
let topp = 0;
let left = 0;
const speed = 5;
document.addEventListener("keydown", (event) => {
  const element = document.getElementById("square");

  if (event.code === "ArrowUp") {
    topp -= speed;
  } else if (event.code === "ArrowDown") {
    topp += speed;
  } else if (event.code === "ArrowLeft") {
    left -= speed;
  } else if (event.code === "ArrowRight") {
    left += speed;
  }

  element.style.top = `${topp}px`;
  element.style.left = `${left}px`;
  console.log(event.code);
});
