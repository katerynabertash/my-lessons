function printSalad(w) {
  console.log(`Для салата весом ${w} гр вам нужно:`);
  function l(ingradient, procent) {
    console.log(`- ${w * procent} гр ${ingradient}`);
  }
  //Сума этих коефициентов равна 1
  l("помидор", 0.45);
  l("огурцов", 0.3);
  l("лука", 0.2);
  l("укроп", 0.05);
}

printSalad(2000);
printSalad(3000);
printSalad(150);
