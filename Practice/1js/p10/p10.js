// создать 5 обьектов людей со свойствами:
// - имя, рост, вес, год рождения, пол

let Kate = {
  name: "Kate",
  growth: 175,
  weight: 125,
  year: 1992,
  gender: "female",
};
let Ilia = {
  name: "Ilia",
  growth: 178,
  weight: 75,
  year: 1989,
  gender: "male",
};
let Vasia = {
  name: "Vasia",
  growth: 187,
  weight: 95,
  year: 1982,
  gender: "male",
};
let Masha = {
  name: "Masha",
  growth: 165,
  weight: 48,
  year: 1987,
  gender: "female",
};
let Nadja = {
  name: "Nadja",
  growth: 168,
  weight: 68,
  year: 1984,
  gender: "female",
};
// написать в консоль для каждого человека:
// - его данные (_имя_ _пол_ _рост_см _вес_кг _год_ года рождения)
// - его индекс массы тела и обьяснение индекса  (толстый\худой\норма)
// - его возвраст и ожидаемый год смерти (на вики найти продолжительность жизни для украинцем муж\жен)
function printManshen(person) {
  console.log(
    `${person.name} ${person.gender} ${person.growth} см ${person.weight} кг ${person.year} года рождения`
  );

  let age = 2022 - person.year;

  let indexMasTel =
    person.weight / ((person.growth / 100) * (person.growth / 100));

  let massLabel = "";
  if (indexMasTel < 24.9) {
    massLabel = "худой";
  } else if (indexMasTel <= 34.9) {
    massLabel = "c нормальной масой тела";
  } else {
    massLabel = "толстый";
  }

  console.log(
    `- ${al(person.gender)} индекс массы тела ${indexMasTel.toFixed(
      2
    )} этот человек ${massLabel}`
  );

  let dedF = person.year + 80;
  let dedM = person.year + 70;
  if (person.gender === "female") {
    console.log(`${al(person)} возвраст ${age} и ожидаемый год смерти ${dedF}`);
  } else {
    console.log(`${al(person)} возвраст ${age} и ожидаемый год смерти ${dedM}`);
  }

  //
  //
  //
  //
  console.log(
    `${al(person)} ${age} лет ожидаемый год смерти ${getDeadYear(person)}`
  );
}

function getDeadYear(person) {
  if (person.gender === "female") {
    return person.year + 80;
  } else {
    return person.year + 70;
  }
}
function al(person) {
  if (person.gender === "male") {
    return "его";
  } else {
    return "ее";
  }
}
printManshen(Kate);
printManshen(Ilia);
printManshen(Vasia);
printManshen(Masha);
printManshen(Nadja);
