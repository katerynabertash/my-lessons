function printIndex(im, label) {
  console.log(`- индекс масси тела ${im.toFixed(1)} у вас ${label}`);
}
function printIndexMassBody(name, growth, weight) {
  let im = weight / (growth * growth);
  console.log(`${name} ваш рост ${growth} метров и вес ${weight} кг`);
  if (im <= 16) printIndex(im, "выраженный дефицит массы тела");
  else if (im <= 18.5) printIndex(im, " недостаточный дефицит массы тела");
  else if (im <= 24.9) printIndex(im, " в норме");
  else if (im <= 30) printIndex(im, " избыточная масса тела (предожирение)");
  else if (im <= 35) printIndex(im, " oжирение первой степени");
  else if (im <= 40) printIndex(im, " oжирение второй степени");
  else if (im > 40) printIndex(im, " oжирение третьей степени");
}

printIndexMassBody("Ilya", 1.78, 10);
printIndexMassBody("Катя", 1.75, 64);
printIndexMassBody("Vasia", 1.68, 50);
