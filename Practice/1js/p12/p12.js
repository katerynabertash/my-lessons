// document.getElementById находим html обьект из страницы
// html обьект:
//      - textContent
//      - innerHTML
//      - addEventListener('ИМЯ_СОБЫТИЯ', функцияОбработчик) - функция для событий (клик)
// classList - обьект для работы с классам

const buttonKatya = document.getElementById("elemK");
const title = document.getElementById("textK");
const buttonIllya = document.getElementById("elemI");

buttonKatya.addEventListener("click", () => {
  title.innerHTML = "Привет Катя!";
});
buttonIllya.addEventListener("click", () => {
  title.innerHTML = "Здраствуй Илья";
});
