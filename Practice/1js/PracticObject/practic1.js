// const product = {
//   name: "Kate",
//   "full name": "Bertash",
//   price: 43,
//   availability: false,
//   "additional gift": null,
// };
// console.log(product);
const student = {
  name: "Kate",
  "last name": "Bertash",
  laziness: 4,
  trick: 5,
};
// // console.log(student);
// student.laziness = 6;
// student.trick = 3;
// function stud() {
//   if (student.laziness >= 5 && student.trick < 4) {
//     // console.log(
//     //   `Студент ${student.name} ${student["last name"]} отправлен на перездачу`
//     // );
//     console.log(
//       `Студент ${student.name} ${student["last name"]} передан в военкомат`
//     );
//   }
// }
// stud();
const danItStudent = {
  name: "Katya",
  "last name": "Bertash",
  "number of works performed": 12,
};
// const inform = prompt(`Что вы хотите узнать о студенте?`);
// console.log(danItStudent[inform]);

const numbers = [1, 4232, 123, 124, 12312, 3];

// for (let o in danItStudent) {
//   console.log(o, danItStudent[o]);
// }

// for (let n of numbers) {
//   console.log(n);
// }

// for (let i = 0; i < numbers.length; i += 1) {
//   console.log(`Элемент под индексом ${i} ${numbers[i]}`);
// }

// let i = 0;
// while (i < numbers.length) {
//   console.log(numbers[i]);
//   i += 1;
// }

numbers.forEach(function (n) {
  console.log(n);
});

numbers.forEach((n) => console.log(n));
