// document.getElementById находим html обьект из страницы
// html обьект:
//      - textContent
//      - innerHTML
//      - addEventListener('ИМЯ_СОБЫТИЯ', функцияОбработчик) - функция для событий (клик)
// classList - обьект для работы с классам

// setName - задать
// getName - достать
const elementBut = document.getElementById("element");
let a = 1;
function f() {
  a += 1;
  elementBut.innerHTML = a;
}

elementBut.addEventListener("click", f);
