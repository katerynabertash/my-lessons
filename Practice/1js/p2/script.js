// // функция getAge с одним параметром и возвращающая результат
// function getAge(year) {
//   return 2022 - year;
// }

// // функция по имени print с двумя параментари и ничего не возвращающая
// function print(firstName, yearBorn) {
//   console.log(firstName + " " + getAge(yearBorn) + " лет");
// }

// // два вызова функции
// print("Ilya", 1989);
// print("Kate", 1992);

// //Привет, Катя, как дела?
// //Привет, Илья, как дела?
// //Привет, Петя, как дела?
// function showWelcome(firstName) {
//   if (firstName !== "Петя") {
//     console.log(`Привет, ${firstName}, как дела?`);
//   } else {
//     console.log(`О ${firstName} привет, как сам?`);
//   }
// }

// showWelcome("Катя");
// showWelcome("Илья");
// showWelcome("Петя");
// showWelcome("Оля");

//Привет, Катя, как дела?
//Привет, Илья, как дела?
//О Петя привет, как сам?
//Привет, Оля, как дела?

//Гвоздики 3х40 = 120грн
//Розы 5х60 = 300грн
//Тюльпаны 1х40 = 40грн

// let totalFlowers = 0;
// let totalCost = 0;

// function printFlower(name, number, price) {
//   let sumPrice = number * price;
//   totalFlowers = totalFlowers + number;
//   totalCost = totalCost + sumPrice;
//   console.log(`${name} ${number} x ${price} = ${sumPrice} грн`);
// }
// function printTotal() {
//   console.log(`Сумма за ${totalFlowers} цветков = ${totalCost} грн`);
//   console.log(`------------------------`);
//   totalFlowers = 0;
//   totalCost = 0;
// }

// printFlower("Гвоздики", 3, 40);
// printFlower("Розы", 5, 60);
// printFlower("Тюльпаны", 1, 40);
// printFlower("Гады", 7, 20);
// printTotal();

// printFlower("Розы", 2, 200);
// printFlower("Тюльпаны", 1, 250);
// printTotal();

// printFlower("Корица", 4, 50);
// printTotal();

// const getArgsCount =function(){
//     return arguments.length;
// }
// console.log(getArgsCount(NaN, null, undefined, 6,'fff', 'string'))

// function countAdvanced(start, end, divide) {
//   if (arguments.length !== 3) {
//     throw new Error("Put three parameters!");
//   }

//   if (
//     !Number.isInteger(start) ||
//     !Number.isInteger(end) ||
//     !Number.isInteger(divide)
//   ) {
//     throw new Error("Put three number!");
//   }
//   if (start > end) {
//     return alert("⛔️ Ошибка! Счёт невозможен.");
//   } else if (start === end) {
//     return alert("⛔️ Ошибка! Нечего считать");
//   }

//   console.log("🏁 Отсчёт начат.");
//   for (let i = start; i <= end; i++) {
//     if (i % divide === 0) {
//       console.log(i);
//     }
//     console.log(i);
//   }
//   console.log("done");
// }

// countAdvanced(5, 2, 7);
//

// function sumArgs() {
//   let sum = 0;
//   for (const argument of arguments) {
//     sum += argument;
//   }
//   return sum;
// }
// console.log(sumArgs(2, 5, 6, 8));

// let count = 0;
// const ingrementn = function () {
//   console.log(count);
//   count += 1;
// };
// ingrement();
// ingrement();

// let number1 = +prompt("ferst namber");
// let number2 = +prompt("second namber");
// function checkIsValid(a) {
//   if (a !== "" && !isNaN && a !== null) {
//     return true;
//   }
//   return false;
// }
// while (checkIsValid(number1)) {
//   number1 = +prompt("ferst namber");
// }
// while (checkIsValid(number2)) {
//   number2 = +prompt("ferst namber");
// }
// console.log(number1 + number2);

// function createIncrement() {
//   let count = 0;
//   return function inner() {
//     count++;
//     return count;
//   };
// }
function createIncrement() {
  let count = 0;
  return function inner() {
    count = 0;
    count++;
    return count;
  };
}
